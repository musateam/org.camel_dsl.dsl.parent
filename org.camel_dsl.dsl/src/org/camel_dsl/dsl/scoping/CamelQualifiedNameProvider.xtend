package org.camel_dsl.dsl.scoping

//MCP

import org.camel_dsl.deployment.CommunicationPort
import org.camel_dsl.deployment.CommunicationPortInstance
import org.camel_dsl.deployment.Component
import org.camel_dsl.deployment.ComponentInstance
import org.camel_dsl.deployment.DeploymentModel
import org.camel_dsl.deployment.HostingPort
import org.camel_dsl.deployment.HostingPortInstance
import org.camel_dsl.deployment.MUSAContainer
import org.camel_dsl.deployment.MUSAPool
import org.camel_dsl.execution.ExecutionContext
import org.camel_dsl.execution.ExecutionModel
import org.camel_dsl.execution.Measurement
import org.camel_dsl.location.CloudLocation
import org.camel_dsl.location.GeographicalRegion
import org.camel_dsl.location.LocationModel
import org.camel_dsl.metric.MetricModel
import org.camel_dsl.metric.Sensor
import org.camel_dsl.organisation.OrganisationModel
import org.camel_dsl.organisation.User
import org.camel_dsl.requirement.Requirement
import org.camel_dsl.requirement.RequirementModel
import org.camel_dsl.security.MUSASecurityControl
import org.camel_dsl.security.SecurityControl
import org.camel_dsl.security.SecurityDomain
import org.camel_dsl.security.SecurityModel
import org.eclipse.xtext.naming.DefaultDeclarativeQualifiedNameProvider
import org.eclipse.xtext.naming.QualifiedName

class CamelQualifiedNameProvider extends DefaultDeclarativeQualifiedNameProvider {
	/* MCP MCP requested 2017/06
	 * en desktop no necesito poner este codigo pero si en web
	 * no entiendo por que lo necesito ya que con VM no es necesario???!!!
	 * me refiero a def qualifiedName(MUSAPool pool), def qualifiedName(MUSAContainer container)
	 * 
	 */
		def qualifiedName(MUSAPool pool){
		System.err.println("MCP CamelQualifiedNameProvider - MUSAPool input name=."+pool.name);
		System.out.println("MCP CamelQualifiedNameProvider - MUSAPool input name=."+pool.name);
		val comp = pool.eContainer as DeploymentModel
		try {
		if (comp == null)
		{
		System.err.println("MCP CamelQualifiedNameProvider - MUSAPool comp is NULL!!!");
		System.out.println("MCP CamelQualifiedNameProvider - MUSAPool comp is NULL!!!");
		}
		else
		{
		System.err.println("MCP CamelQualifiedNameProvider - MUSAPool comp=."+comp.name);
		System.out.println("MCP CamelQualifiedNameProvider - MUSAPool comp=."+comp.name);
		}
		}
		catch (Exception e)
		{
		System.err.println("MCP CamelQualifiedNameProvider - MUSAPool exception ");
		System.out.println("MCP CamelQualifiedNameProvider - MUSAPool exception ");
		System.err.println(e.getMessage());
		System.out.println(e.getMessage());
		}
		//return QualifiedName.create(comp.name, pool.name)
		//return QualifiedName.create(pool.name, pool.name)
		return QualifiedName.create(pool.name)
	}

	def qualifiedName(MUSAContainer container){
		System.err.println("MCP CamelQualifiedNameProvider - MUSAContainer input name=."+container.name);
		System.out.println("MCP CamelQualifiedNameProvider - MUSAContainer input name=."+container.name);
		val comp = container.eContainer as DeploymentModel
		try {
		if (comp == null)
		{
		System.err.println("MCP CamelQualifiedNameProvider - MUSAContainer comp is NULL!!!");
		System.out.println("MCP CamelQualifiedNameProvider - MUSAContainer comp is NULL!!!");
		}
		else
		{
		System.err.println("MCP CamelQualifiedNameProvider - MUSAContainer comp=."+comp.name);
		System.out.println("MCP CamelQualifiedNameProvider - MUSAContainer comp=."+comp.name);
		}
		}
		catch (Exception e)
		{
		System.err.println("MCP CamelQualifiedNameProvider - MUSAPool exception ");
		System.out.println("MCP CamelQualifiedNameProvider - MUSAPool exception ");
		System.err.println(e.getMessage());
		System.out.println(e.getMessage());
		}
		//return QualifiedName.create(comp.name, container.name)
		//return QualifiedName.create(container.name, container.name)
		return QualifiedName.create(container.name)
	}
//End MCP requested 2017/06	
	
	def qualifiedName(CommunicationPort port){
		val comp = port.eContainer as Component
		return QualifiedName.create(comp.name, port.name)
	}
	
	def qualifiedName(HostingPort port){
		val comp = port.eContainer as Component
		return QualifiedName.create(comp.name, port.name)
	}
	
	def qualifiedName(CommunicationPortInstance port){
		val ci = port.eContainer as ComponentInstance
		return QualifiedName.create(ci.name, port.name)
	}
	
	def qualifiedName(HostingPortInstance port){
		val ci = port.eContainer as ComponentInstance
		return QualifiedName.create(ci.name, port.name)
	}
	
	def qualifiedName(Requirement req){
		val rm = req.eContainer as RequirementModel
		return QualifiedName.create(rm.name, req.name)
	}
	
	def qualifiedName(User u){
		val org = u.eContainer as OrganisationModel
		return QualifiedName.create(org.name,u.name)
	}
	
	def qualifiedName(Sensor s){
		val mm = s.eContainer as MetricModel
		return QualifiedName.create(mm.name,s.name)
	}
	
	def qualifiedName(ExecutionContext ec){
		val mm = ec.eContainer as ExecutionModel
		return QualifiedName.create(mm.name,ec.name)
	}
	
	def qualifiedName(Measurement m){
		val mm = m.eContainer as ExecutionModel
		return QualifiedName.create(mm.name,m.name)
	}
	
	def qualifiedName(SecurityControl sc){
		val sm = sc.eContainer as SecurityModel
		return QualifiedName.create(sm.name,sc.name)
	}
	
	def qualifiedName(SecurityDomain sd){
		val sm = sd.eContainer as SecurityModel
		return QualifiedName.create(sm.name,sd.id)
	}

	def qualifiedName(MUSASecurityControl sc){
		val sm = sc.eContainer as SecurityModel
		//System.err.println("MCP CamelQualifiedNameProvider - MUSASecurityControl name=."+sm.name + ". id=." + sc.id);
		//System.out.println("MCP CamelQualifiedNameProvider - MUSASecurityControl name=."+sm.name + ". id=." + sc.id);
		return QualifiedName.create(sm.name,sc.id)
	}

	
	def qualifiedName(CloudLocation cl){
		val lm = cl.eContainer
		if (lm instanceof CloudLocation){
			return QualifiedName.create(lm.id,cl.id);	
		}
		else{
			val lm2 = lm as LocationModel
			return QualifiedName.create(lm2.name,cl.id);	
		}
	}
	
	def qualifiedName(GeographicalRegion l){
		val lm = l.eContainer as LocationModel
		return QualifiedName.create(lm.name,l.id)
	}
}