/*
 * generated by Xtext 2.9.1
 */
package org.camel_dsl.dsl.scoping


/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class CamelDslScopeProvider extends AbstractCamelDslScopeProvider {

}
