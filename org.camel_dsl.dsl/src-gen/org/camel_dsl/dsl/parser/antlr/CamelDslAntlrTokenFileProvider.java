/*
 * generated by Xtext 2.12.0
 */
package org.camel_dsl.dsl.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class CamelDslAntlrTokenFileProvider implements IAntlrTokenFileProvider {

	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		return classLoader.getResourceAsStream("org/camel_dsl/dsl/parser/antlr/internal/InternalCamelDsl.tokens");
	}
}
