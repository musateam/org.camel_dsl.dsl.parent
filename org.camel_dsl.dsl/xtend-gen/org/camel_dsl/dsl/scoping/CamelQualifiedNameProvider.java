package org.camel_dsl.dsl.scoping;

import com.google.common.base.Objects;
import org.camel_dsl.deployment.CommunicationPort;
import org.camel_dsl.deployment.CommunicationPortInstance;
import org.camel_dsl.deployment.Component;
import org.camel_dsl.deployment.ComponentInstance;
import org.camel_dsl.deployment.DeploymentModel;
import org.camel_dsl.deployment.HostingPort;
import org.camel_dsl.deployment.HostingPortInstance;
import org.camel_dsl.deployment.MUSAContainer;
import org.camel_dsl.deployment.MUSAPool;
import org.camel_dsl.execution.ExecutionContext;
import org.camel_dsl.execution.ExecutionModel;
import org.camel_dsl.execution.Measurement;
import org.camel_dsl.location.CloudLocation;
import org.camel_dsl.location.GeographicalRegion;
import org.camel_dsl.location.LocationModel;
import org.camel_dsl.metric.MetricModel;
import org.camel_dsl.metric.Sensor;
import org.camel_dsl.organisation.OrganisationModel;
import org.camel_dsl.organisation.User;
import org.camel_dsl.requirement.Requirement;
import org.camel_dsl.requirement.RequirementModel;
import org.camel_dsl.security.MUSASecurityControl;
import org.camel_dsl.security.SecurityControl;
import org.camel_dsl.security.SecurityDomain;
import org.camel_dsl.security.SecurityModel;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.DefaultDeclarativeQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.lib.Exceptions;

@SuppressWarnings("all")
public class CamelQualifiedNameProvider extends DefaultDeclarativeQualifiedNameProvider {
  /**
   * MCP MCP requested 2017/06
   * en desktop no necesito poner este codigo pero si en web
   * no entiendo por que lo necesito ya que con VM no es necesario???!!!
   * me refiero a def qualifiedName(MUSAPool pool), def qualifiedName(MUSAContainer container)
   */
  public QualifiedName qualifiedName(final MUSAPool pool) {
    String _name = pool.getName();
    String _plus = ("MCP CamelQualifiedNameProvider - MUSAPool input name=." + _name);
    System.err.println(_plus);
    String _name_1 = pool.getName();
    String _plus_1 = ("MCP CamelQualifiedNameProvider - MUSAPool input name=." + _name_1);
    System.out.println(_plus_1);
    EObject _eContainer = pool.eContainer();
    final DeploymentModel comp = ((DeploymentModel) _eContainer);
    try {
      boolean _equals = Objects.equal(comp, null);
      if (_equals) {
        System.err.println("MCP CamelQualifiedNameProvider - MUSAPool comp is NULL!!!");
        System.out.println("MCP CamelQualifiedNameProvider - MUSAPool comp is NULL!!!");
      } else {
        String _name_2 = comp.getName();
        String _plus_2 = ("MCP CamelQualifiedNameProvider - MUSAPool comp=." + _name_2);
        System.err.println(_plus_2);
        String _name_3 = comp.getName();
        String _plus_3 = ("MCP CamelQualifiedNameProvider - MUSAPool comp=." + _name_3);
        System.out.println(_plus_3);
      }
    } catch (final Throwable _t) {
      if (_t instanceof Exception) {
        final Exception e = (Exception)_t;
        System.err.println("MCP CamelQualifiedNameProvider - MUSAPool exception ");
        System.out.println("MCP CamelQualifiedNameProvider - MUSAPool exception ");
        System.err.println(e.getMessage());
        System.out.println(e.getMessage());
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
    return QualifiedName.create(pool.getName());
  }
  
  public QualifiedName qualifiedName(final MUSAContainer container) {
    String _name = container.getName();
    String _plus = ("MCP CamelQualifiedNameProvider - MUSAContainer input name=." + _name);
    System.err.println(_plus);
    String _name_1 = container.getName();
    String _plus_1 = ("MCP CamelQualifiedNameProvider - MUSAContainer input name=." + _name_1);
    System.out.println(_plus_1);
    EObject _eContainer = container.eContainer();
    final DeploymentModel comp = ((DeploymentModel) _eContainer);
    try {
      boolean _equals = Objects.equal(comp, null);
      if (_equals) {
        System.err.println("MCP CamelQualifiedNameProvider - MUSAContainer comp is NULL!!!");
        System.out.println("MCP CamelQualifiedNameProvider - MUSAContainer comp is NULL!!!");
      } else {
        String _name_2 = comp.getName();
        String _plus_2 = ("MCP CamelQualifiedNameProvider - MUSAContainer comp=." + _name_2);
        System.err.println(_plus_2);
        String _name_3 = comp.getName();
        String _plus_3 = ("MCP CamelQualifiedNameProvider - MUSAContainer comp=." + _name_3);
        System.out.println(_plus_3);
      }
    } catch (final Throwable _t) {
      if (_t instanceof Exception) {
        final Exception e = (Exception)_t;
        System.err.println("MCP CamelQualifiedNameProvider - MUSAPool exception ");
        System.out.println("MCP CamelQualifiedNameProvider - MUSAPool exception ");
        System.err.println(e.getMessage());
        System.out.println(e.getMessage());
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
    return QualifiedName.create(container.getName());
  }
  
  public QualifiedName qualifiedName(final CommunicationPort port) {
    EObject _eContainer = port.eContainer();
    final Component comp = ((Component) _eContainer);
    return QualifiedName.create(comp.getName(), port.getName());
  }
  
  public QualifiedName qualifiedName(final HostingPort port) {
    EObject _eContainer = port.eContainer();
    final Component comp = ((Component) _eContainer);
    return QualifiedName.create(comp.getName(), port.getName());
  }
  
  public QualifiedName qualifiedName(final CommunicationPortInstance port) {
    EObject _eContainer = port.eContainer();
    final ComponentInstance ci = ((ComponentInstance) _eContainer);
    return QualifiedName.create(ci.getName(), port.getName());
  }
  
  public QualifiedName qualifiedName(final HostingPortInstance port) {
    EObject _eContainer = port.eContainer();
    final ComponentInstance ci = ((ComponentInstance) _eContainer);
    return QualifiedName.create(ci.getName(), port.getName());
  }
  
  public QualifiedName qualifiedName(final Requirement req) {
    EObject _eContainer = req.eContainer();
    final RequirementModel rm = ((RequirementModel) _eContainer);
    return QualifiedName.create(rm.getName(), req.getName());
  }
  
  public QualifiedName qualifiedName(final User u) {
    EObject _eContainer = u.eContainer();
    final OrganisationModel org = ((OrganisationModel) _eContainer);
    return QualifiedName.create(org.getName(), u.getName());
  }
  
  public QualifiedName qualifiedName(final Sensor s) {
    EObject _eContainer = s.eContainer();
    final MetricModel mm = ((MetricModel) _eContainer);
    return QualifiedName.create(mm.getName(), s.getName());
  }
  
  public QualifiedName qualifiedName(final ExecutionContext ec) {
    EObject _eContainer = ec.eContainer();
    final ExecutionModel mm = ((ExecutionModel) _eContainer);
    return QualifiedName.create(mm.getName(), ec.getName());
  }
  
  public QualifiedName qualifiedName(final Measurement m) {
    EObject _eContainer = m.eContainer();
    final ExecutionModel mm = ((ExecutionModel) _eContainer);
    return QualifiedName.create(mm.getName(), m.getName());
  }
  
  public QualifiedName qualifiedName(final SecurityControl sc) {
    EObject _eContainer = sc.eContainer();
    final SecurityModel sm = ((SecurityModel) _eContainer);
    return QualifiedName.create(sm.getName(), sc.getName());
  }
  
  public QualifiedName qualifiedName(final SecurityDomain sd) {
    EObject _eContainer = sd.eContainer();
    final SecurityModel sm = ((SecurityModel) _eContainer);
    return QualifiedName.create(sm.getName(), sd.getId());
  }
  
  public QualifiedName qualifiedName(final MUSASecurityControl sc) {
    EObject _eContainer = sc.eContainer();
    final SecurityModel sm = ((SecurityModel) _eContainer);
    return QualifiedName.create(sm.getName(), sc.getId());
  }
  
  public QualifiedName qualifiedName(final CloudLocation cl) {
    final EObject lm = cl.eContainer();
    if ((lm instanceof CloudLocation)) {
      return QualifiedName.create(((CloudLocation)lm).getId(), cl.getId());
    } else {
      final LocationModel lm2 = ((LocationModel) lm);
      return QualifiedName.create(lm2.getName(), cl.getId());
    }
  }
  
  public QualifiedName qualifiedName(final GeographicalRegion l) {
    EObject _eContainer = l.eContainer();
    final LocationModel lm = ((LocationModel) _eContainer);
    return QualifiedName.create(lm.getName(), l.getId());
  }
}
