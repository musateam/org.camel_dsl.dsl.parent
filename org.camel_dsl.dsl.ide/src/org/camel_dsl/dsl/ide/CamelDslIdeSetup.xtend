/*
 * generated by Xtext 2.12.0
 */
package org.camel_dsl.dsl.ide

import com.google.inject.Guice
import org.camel_dsl.dsl.CamelDslRuntimeModule
import org.camel_dsl.dsl.CamelDslStandaloneSetup
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class CamelDslIdeSetup extends CamelDslStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new CamelDslRuntimeModule, new CamelDslIdeModule))
	}
	
}
