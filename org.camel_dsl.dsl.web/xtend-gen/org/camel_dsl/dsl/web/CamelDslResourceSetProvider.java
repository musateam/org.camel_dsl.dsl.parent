/**
 * Copyright (c) 2015 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.camel_dsl.dsl.web;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import com.google.inject.Provider;
import java.io.FileInputStream;
import java.util.Scanner;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.util.StringInputStream;
import org.eclipse.xtext.web.server.IServiceContext;
import org.eclipse.xtext.web.server.ISession;
import org.eclipse.xtext.web.server.model.IWebResourceSetProvider;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function0;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Pair;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

/**
 * Resources that run in multi-resource mode share the same resource set.
 */
@SuppressWarnings("all")
public class CamelDslResourceSetProvider implements IWebResourceSetProvider {
  private final static String MULTI_RESOURCE_PREFIX = "multi-resource";
  
  @Inject
  private Provider<ResourceSet> provider;
  
  private final static String fileExtension = ".camel";
  
  private final static String UTF_8 = "UTF-8";
  
  @Override
  public ResourceSet get(final String resourceId, final IServiceContext serviceContext) {
    ResourceSet _xblockexpression = null;
    {
      ResourceSet _xifexpression = null;
      if (((resourceId != null) && resourceId.startsWith(CamelDslResourceSetProvider.MULTI_RESOURCE_PREFIX))) {
        ResourceSet _xblockexpression_1 = null;
        {
          final int pathEnd = Math.max(resourceId.indexOf("/"), CamelDslResourceSetProvider.MULTI_RESOURCE_PREFIX.length());
          ISession _session = serviceContext.getSession();
          String _substring = resourceId.substring(0, pathEnd);
          Pair<Class<ResourceSet>, String> _mappedTo = Pair.<Class<ResourceSet>, String>of(ResourceSet.class, _substring);
          final Function0<ResourceSet> _function = () -> {
            return this.provider.get();
          };
          _xblockexpression_1 = _session.<ResourceSet>get(_mappedTo, _function);
        }
        _xifexpression = _xblockexpression_1;
      } else {
        _xifexpression = this.provider.get();
      }
      final ResourceSet result = _xifexpression;
      final Procedure1<ResourceSet> _function = (ResourceSet it) -> {
        try {
          System.err.println("1111");
          String varResourceBaseProviderDB = "/home/MUSA/MODELLER/db-files";
          boolean _contains = System.getProperty("os.name").contains("Windows");
          if (_contains) {
            varResourceBaseProviderDB = "D:/MUSA/db-files";
          }
          final String resourceBaseProviderDB = varResourceBaseProviderDB;
          final Resource ry = it.getResource(URI.createURI(((resourceBaseProviderDB + "/MUSAAGENTS") + CamelDslResourceSetProvider.fileExtension)), false);
          boolean _notEquals = (!Objects.equal(ry, null));
          if (_notEquals) {
            String _string = ry.toString();
            String _plus = ("URI=" + _string);
            System.err.println(_plus);
            ry.load(null);
          }
          boolean _equals = Objects.equal(ry, null);
          if (_equals) {
            final FileInputStream fileStream = new FileInputStream(((resourceBaseProviderDB + "/MUSAAGENTS") + CamelDslResourceSetProvider.fileExtension));
            String _string_1 = fileStream.toString();
            String _plus_1 = ("CamelDslResourceSetProvider.get: fileStream:=." + _string_1);
            String _plus_2 = (_plus_1 + ".");
            System.out.println(_plus_2);
            final String _text = new Scanner(fileStream, CamelDslResourceSetProvider.UTF_8).useDelimiter("\\A").next();
            fileStream.close();
            System.out.println(("CamelDslResourceSetProvider.get: _textXMI=" + _text));
            final Resource r = it.createResource(URI.createURI((resourceBaseProviderDB + "/MUSAAGENTS.camel")));
            boolean _notEquals_1 = (!Objects.equal(r, null));
            if (_notEquals_1) {
              String _string_2 = r.toString();
              String _plus_3 = ("r=" + _string_2);
              System.err.println(_plus_3);
            }
            final StringInputStream in = new StringInputStream((("" + _text) + ""));
            r.load(in, null);
            EList<EObject> _contents = r.getContents();
            String _plus_4 = (_contents + ".2222");
            InputOutput.<String>println(_plus_4);
          }
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      };
      ObjectExtensions.<ResourceSet>operator_doubleArrow(result, _function);
      _xblockexpression = result;
    }
    return _xblockexpression;
  }
}
