/**
 * generated by Xtext 2.9.1
 */
package org.camel_dsl.dsl.web;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import javax.servlet.annotation.WebServlet;
import org.eclipse.xtext.web.servlet.XtextServlet;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

/**
 * Deploy this class into a servlet container to enable DSL-specific services.
 */
@WebServlet(name = "XtextServices", urlPatterns = "/xtext-service/*")
@SuppressWarnings("all")
public class CamelDslServlet extends XtextServlet {
  private final List<ExecutorService> executorServices = CollectionLiterals.<ExecutorService>newArrayList();
  
  @Override
  public void init() {
    throw new Error("Unresolved compilation problems:"
      + "\nInvalid number of arguments. The constructor CamelDslWebModule(IResourceBaseProvider, boolean) is not applicable for the arguments (Provider<ExecutorService>,ResourceBaseProviderImpl,boolean)"
      + "\nInvalid number of arguments. The constructor CamelDslWebModule(IResourceBaseProvider, boolean) is not applicable for the arguments (Provider<ExecutorService>,ResourceBaseProviderImpl,boolean)"
      + "\nType mismatch: cannot convert from Provider<ExecutorService> to IResourceBaseProvider"
      + "\nType mismatch: cannot convert from ResourceBaseProviderImpl to boolean"
      + "\nType mismatch: cannot convert from Provider<ExecutorService> to IResourceBaseProvider"
      + "\nType mismatch: cannot convert from ResourceBaseProviderImpl to boolean");
  }
  
  @Override
  public void destroy() {
    final Consumer<ExecutorService> _function = (ExecutorService it) -> {
      it.shutdown();
    };
    this.executorServices.forEach(_function);
    this.executorServices.clear();
    super.destroy();
  }
}
